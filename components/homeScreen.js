import * as React from 'react';
import { View, StyleSheet, Dimensions,Text,Button } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';



const FirstRoute = () => (
  <View style={[styles.scene, { backgroundColor: '#ff4081' }]}>
  </View>
);
 
const SecondRoute = () => (
  <View style={[styles.scene, { backgroundColor: '#673ab7' }]} />
);
const thirdRoute = () => (
  <View style={[styles.scene, { backgroundColor: 'pink' }]} />
);

const initialLayout = { width: Dimensions.get('window').width };

function HomeScreen({}) {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'first', title: 'CALLS' },
    { key: 'second', title: 'CHATS' },
    { key: 'third', title: 'CONTACTS' },
  ]);
 
  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
    third: thirdRoute,
  });
    return (
      <TabView
      navigationState={{ index, routes }}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={initialLayout}
    />
      // <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      //   <Text>Home Screen</Text>
      //   <Button title="go to detail page" onPress={()=>navigation.navigate('Scanner')}/>
      // </View>
    );
  }
  const styles = StyleSheet.create({
    scene: {
      flex: 1,
    },
  });
  export default HomeScreen
 